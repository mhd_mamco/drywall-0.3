'use strict';

exports = module.exports = function (app) {
  app.utility = {};
  app.utility.sendmail = require('drywall-sendmail');
  app.utility.slugify = require('drywall-slugify');
  app.utility.workflow = require('drywall-workflow');
  app.utility.adminRole = require('drywall-Admins');
};