'use strict';

//dependencies
var config = require('config');
var express = require('express');
var mongoStore = require('connect-mongo')(express);
var http = require('http');
var path = require('path');
var passport = require('passport');

//create express app
var app = express();

//setup the web server
app.server = http.createServer(app);

// * Setting up MonogDB
require('./setup/mongoose')(app, config);

// * Setting up Express server variables
require('./setup/express')(app, config, passport);

//config data models
require('./models')(app);

//setup the session store
app.sessionStore = new mongoStore({
  url: config.mongodb.uri
});

//setup passport
require('./passport')(app, passport);

//route requests
require('./routes')(app, passport);

//setup utilities
require('./utility')(app);

//listen up
app.server.listen(app.get('port'), function () {
  //and... we're live
});