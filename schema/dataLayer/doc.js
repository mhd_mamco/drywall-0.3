'use strict';

exports = module.exports = function (app, mongoose) {
	var schema = new mongoose.Schema({
		userName: {
			type: Number,
			default: ''
		},
		email: {
			type: String,
			default: 0
		}
	}, {
		autoIndex: false
	}, {
		versionKey: false
	});


	schema.plugin(require('../plugins/pagedFind'));
	schema.index({
		email: 1
	});
	app.db.model('doc', schema);
};