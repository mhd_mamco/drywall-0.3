// MongoDB setup
// =============

// Server dependencies
// -------------------
// ###NPM
// * Mongoose.js

var mongoose = require('mongoose');

module.exports = function(app, config) {
    console.log('\nPreparing MongoDB');


    //setup mongoose
    app.db = mongoose.createConnection(config.mongodb.uri);
   
    app.db.on('error', function(error) {
        console.error('\nError in MONGODB connection: ' + error);
        console.error('\nUnable to connect to database at ' + config.mongodb.uri);
        mongoose.disconnect();
    });

    app.db.on('connected', function() {
        console.log('\nMONGODB connected at ' + config.mongodb.uri);
    });

    app.db.once('open', function() {
        console.log('\nMONGODB connection opened!');
    });

    app.db.on('reconnected', function() {
        console.log('\nMONGODB reconnected!');
    });

    app.db.on('disconnected', function() {
        setTimeout(function() {
            console.error('\nMONGODB disconnected!');
            console.error('\nMONGODB: Reconnecting in: ' + config.mongodb.Time_Out + ' milliseconds');
            mongoose.connect(config.mongodb.uri, config.mongodb.DB_OPTIONS);
        }, config.mongodb.Time_Out);
    });
};