'use strict';

angular.module('drywall').controller('DocCtrl',
  function ($scope, docsService) {

    // Page will be NaN if we don't set it to 1 at first
    $scope.filters = {
      page: 1
    };

    function loadData(filters) {
      docsService.load(filters).then(function (response) {
        // Make sure the current filters correspond to this particular load
        if (filters == $scope.filters) {
          $scope.results = response.data;
          $scope.pages = response.pages;
          $scope.items = response.items;
        }
      }, function (err) {
        alert('There was an error loading docs values: ' + err);
      });
    }
    var errMsg = 'you have the following problem(s): ';

    function resetFields() {
      $scope.userName = '';
      $scope.email = '';
    }

    function closeAllResults() {
      _.each($scope.results, function (result) {
        result.details = null;
      });
    }
    // expose closeAllResults to frontEnd
    $scope.close = function () {
      closeAllResults();
    };

    $scope.$watch('filters', function (nv, ov) {
      loadData(nv);
    }, true);

    $scope.addNew = function () {
      console.log($scope);
      var doc = {
        userName: $scope.userName,
        email: $scope.email
      };
      docsService.create(doc).then(_.bind(loadData, null, $scope.filters));
      resetFields();
    };

    $scope.nextPage = function () {
      if ($scope.pages.hasNext) {
        $scope.filters.page += 1;
      }
    };

    $scope.previousPage = function () {
      if ($scope.pages.hasPrev) {
        $scope.filters.page -= 1;
      }
    };

    $scope.edit = function () {
      closeAllResults();
      var that = this;

      docsService.get(this.doc._id).then(function (data) {
        that.doc.details = data;
      });
    };

    $scope.save = function () {

      docsService.update(this.doc.details).then(function (data) {
        if (data.success) {
          loadData($scope.filters)
        } else if (data.errors) {
          alert(errMsg + data.errors.join(',\n'));
        }
        closeAllResults();

      });
    };

    $scope.delete = function () {
      var result = confirm('you are about to remove ' + this.doc.userName + '\nconfirm');
      if (result) {
        docsService.delete(this.doc._id).then(function (data) {
          if (data.success) {
            loadData($scope.filters);
          } else if (data.errors) {
            alert(errMsg + data.errors.join(',\n'));
          }
        });
      }
    };
  }
);