'use strict';

angular.module('drywall').factory('docsService',
  function (ResourcesService) {
    var Urls = {
      LOAD: '/admin/doc/',
      CREATE: '/admin/doc/',
      GET: _.template('/admin/doc/<%= id %>'),
      UPDATE: _.template('/admin/doc/<%= id %>'),
      DELETE: _.template('/admin/doc/<%= id %>'),
      UPLOAD_RECOURSE: _.template('/admin/doc/uploadRecourse/<%= id %>'),
      DOWNLOAD_RECOURSE: _.template('/admin/doc/downloadRecourse/<%= id %>'),
      DELETE_RECOURSE: _.template('/admin/doc/deleteRecourse/<%= id %>')
    };
    return ResourcesService(Urls);
  }
);