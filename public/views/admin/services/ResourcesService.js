angular.module('drywall').factory('ResourcesService', ['$q', '$http',
  function ($q, $http) {

    return function (Urls) {
      return {
        load: function (options) {
          return $http.get(Urls.LOAD, {
            params: options,
            headers: {
              'X-Requested-With': 'XMLHttpRequest'
            }
          }).then(function (response) {
            return response.data;
          });
        },
        create: function (data) {
          return $http.post(Urls.CREATE, data).then(function (response) {
            return response.data;
          });
        },
        get: function (id) {
          return $http.get(Urls.GET({
            id: id
          })).then(function (response) {
            return response.data;
          });
        },
        update: function (data) {
          return $http.put(Urls.UPDATE({
            id: data._id
          }), data).then(function (response) {
            return response.data;
          });
        },
        delete: function (id) {
          return $http.delete(Urls.DELETE({
            id: id
          })).then(function (response) {
            return response.data;
          });
        },
        deleteRecourse: function (options) {
          return $http.put(Urls.DELETE_RECOURSE({
            id: options.id,
            type: options.type
          })).then(function (response) {
            return response.data;
          });
        },
        uploadRecourse: function (options) {
          return $http.post(Urls.UPLOAD_RECOURSE({
            id: options.id,
            type: options.type
          }), options.file, {
            withCredentials: true,
            headers: {
              'Content-Type': undefined
            },
            transformRequest: angular.identity
          }).then(function (response) {
            console.log(response);
            return response.data;
          });
        },
        downloadRecourse: function (options) {
          return $http.get(Urls.DOWNLOAD_RECOURSE({
            id: options.id,
            type: options.type
          })).then(function (response) {
            return response.data;
          });
        },
      };
    };
  }
]);