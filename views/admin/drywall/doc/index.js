'use strict';

var _ = require('lodash');

exports.find = function (req, res, next) {
	// Normal get displays the page
	if (!req.xhr) {
		res.render('admin/drywall/doc/index');
	} else {
		//defaults
		// req.query.title = req.query.title ? req.query.title : '';

		req.query = _.defaults(req.query, {
			userName: '',
			limit: 20,
			page: 1,
			sort: '_id'
		});
		//filters
		var filters = {};
		if (req.query.userName) {
			filters.userName = new RegExp('^.*?' + req.query.userName + '.*$', 'i');
		}
		//get results
		req.app.db.models.doc.pagedFind({
			filters: filters,
			keys: 'userName email',
			limit: req.query.limit,
			page: req.query.page,
			sort: req.query.sort
		}, function (err, results) {
			if (err) {
				return next(err);
			}
			res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
			results.filters = req.query;
			res.send(results);
		});
	}
};


exports.read = function (req, res, next) {
	req.app.db.models.doc.findById(req.params.id).exec(function (err, zakat) {
		if (err) {
			return next(err);
		}
		res.send(zakat);
	});
};


exports.create = function (req, res, next) {
	var workflow = new req.app.utility.workflow(req, res);

	workflow.on('validate', function () {

		if (!req.user.roles.admin.isMemberOf('root')) {
			workflow.outcome.errors.push('You may not create zakat.');
			return workflow.emit('response');
		}

		if (!req.body.email) {
			workflow.outcome.errors.push('An email is required.');
			return workflow.emit('response');
		}

		if (!req.body.userName) {
			workflow.outcome.errors.push('A userName is required.');
			return workflow.emit('response');
		}

		workflow.emit('duplicateuserNameCheck');
	});

	workflow.on('duplicateuserNameCheck', function () {
		req.app.db.models.doc.findOne({
			userName: req.body.userName
		}).exec(function (err, doc) {
			if (err) {
				return workflow.emit('exception', err);
			}
			if (doc) {
				workflow.outcome.errors.push('That userName is already taken.');
				return workflow.emit('response');
			}

			workflow.emit('createZakat');
		});
	});


	workflow.on('createZakat', function () {
		var fieldsToSet = {
			userName: req.body.userName,
			email: req.body.email
		};
		req.app.db.models.doc.create(fieldsToSet, function (err, zakat) {
			if (err) {
				return workflow.emit('exception', err);
			}
			workflow.outcome.record = zakat;
			return workflow.emit('response');
		});
	});

	workflow.emit('validate');
};

exports.update = function (req, res, next) {

	var workflow = new req.app.utility.workflow(req, res);

	workflow.on('validate', function () {

		if (!req.user.roles.admin.isMemberOf('root')) {
			workflow.outcome.errors.push('You may not update zakat.');
			return workflow.emit('response');
		}

		if (!req.body.email) {
			workflow.outcome.errfor.title = 'email';
			return workflow.emit('response');
		}

		if (!req.body.userName) {
			workflow.outcome.errfor.title = 'userName';
			return workflow.emit('response');
		}
		workflow.emit('patchZakat');
	});


	workflow.on('patchZakat', function () {
		var fieldsToSet = {
			userName: req.body.userName,
			email: req.body.email
		};

		req.app.db.models.doc.findByIdAndUpdate(req.body._id, fieldsToSet, function (err, zakat) {
			if (err) {
				return workflow.emit('exception', err);
			}
			workflow.outcome.record = zakat;
			return workflow.emit('response');
		});
	});

	workflow.emit('validate');
};



exports.delete = function (req, res, next) {
	var workflow = new req.app.utility.workflow(req, res);

	workflow.on('validate', function () {

		if (!req.user.roles.admin.isMemberOf('root')) {
			workflow.outcome.errors.push('You may not delete zakat.');
			return workflow.emit('response');
		}

		workflow.emit('deleteZakat');
	});

	workflow.on('deleteZakat', function (err) {
		req.app.db.models.doc.findByIdAndRemove(req.params.id, function (err, zakat) {
			if (err) {
				return workflow.emit('exception', err);
			}
			workflow.emit('response');
		});
	});

	workflow.emit('validate');
};