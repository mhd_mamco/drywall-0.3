Drywall
=============

A website and user system for Node.js. What you create with Drywall is more important than Drywall. [See a bird's eye view.](http://jedireza.github.io/drywall/)

[![Dependency Status](https://david-dm.org/jedireza/drywall.png)](https://david-dm.org/jedireza/drywall)
[![devDependency Status](https://david-dm.org/jedireza/drywall/dev-status.png)](https://david-dm.org/jedireza/drywall#info=devDependencies)

Technology
------------

| On The Server | On The Client  | Development |
| ------------- | -------------- | ----------- |
| Express       | Bootstrap      | Grunt       |
| Jade          | Backbone.js    |             |
| Mongoose      | jQuery         |             |
| Passport      | Underscore.js  |             |
| Async         | Font-Awesome   |             |
| EmailJS       | Moment.js      |             |
| lodash        | Angular.js     |             |

Requirements
------------

You need [Node.js](http://nodejs.org/download/) and [MongoDB](http://www.mongodb.org/downloads) installed and running.

We use [Grunt](http://gruntjs.com/) as our task runner. Get the CLI (command line interface).

```bash
$ npm install -g grunt-cli
```

Installation
------------

```bash
$ git clone git@github.com:jedireza/drywall.git && cd ./drywall
$ npm install
$ bower install
$ grunt
```

Setup
------------

You need a few records in the database to start using the user system.

Run these commands on mongo. __Obviously you should use your email address.__

```js
use drywall;

db.admingroups.insert({ _id: 'root', name: 'Root' });
db.admingroups.insert({ _id: 'super-admin', name: 'Super-Admin' });
db.admingroups.insert({ _id: 'admin', name: 'Admin' });

db.admins.insert({ name: {first: 'Root', last: 'Admin', full: 'Root Admin'}, groups: ['root'] });
db.admins.insert({ name: {first: 'Supper', last: 'Admin', full: 'Supper Admin'}, groups: ['super-admin'] });
db.admins.insert({ name: {first: 'Admin', last: 'Admin', full: 'Admin Admin'}, groups: ['admin'] });

var rootAdmin = db.admins.findOne({'name.first':'Root'});
var superAdmin = db.admins.findOne({'name.first':'Supper'});
var admin = db.admins.findOne({'name.first':'Admin'});

db.users.save({ username: 'root', isActive: 'yes', email: 'root@email.addy', roles: {admin: rootAdmin._id} });
db.users.save({ username: 'super', isActive: 'yes', email: 'super@email.addy', roles: {admin: superAdmin._id} });
db.users.save({ username: 'admin', isActive: 'yes', email: 'admin@email.addy', roles: {admin: admin._id} });

var rootUser = db.users.find({'username':'root'});
var superAdminUser = db.users.find({'username':'super-admin'});
var adminUser = db.users.find({'username':'admin'});

rootAdmin.user = { id: rootUser._id, name: rootUser.username };
superAdmin.user = { id: superAdminUser._id, name: superAdminUser.username };
admin.user = { id: adminUser._id, name: adminUser.username };

db.admins.save(rootAdmin);
db.admins.save(superAdmin);
db.admins.save(admin);

```

Now just use the reset password feature to set a password.

 - `http://localhost:3000/login/forgot/`
 - Submit your email address and wait a second.
 - Go check your email and get the reset link.
 - `http://localhost:3000/login/reset/:token/`
 - Set a new password.

Login. Customize. Enjoy.

Philosophy
------------

 - Create a website and user system.
 - Write code in a simple and consistent way.
 - Only create minor utilities or plugins to avoid repetitiveness.
 - Find and use good tools.
 - Use tools in their native/default behavior.

Features
------------

 - Basic front end web pages.
 - Contact page has form to email.
 - Login system with forgot password and reset password.
 - Signup and Login with Facebook, Twitter and GitHub.
 - Optional email verification during signup flow.
 - User system with separate account and admin roles.
 - Admin groups with shared permission settings.
 - Administrator level permissions that override group permissions.
 - Global admin quick search component.

License
------------

MIT

[![githalytics.com alpha](https://cruel-carlota.pagodabox.com/d41f60f22a2148e2e2dc6b705cd01481 "githalytics.com")](http://githalytics.com/jedireza/drywall)
